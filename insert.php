<?php

$user = 'root';
$password = 'root';

$db = new PDO('mysql:host=localhost;dbname=contacts', $user, $password);

$sql = 'INSERT INTO contacts (name, email) VALUES (:name, :email)';

$statement = $db->prepare($sql);
$statement->bindParam('name', $_POST['name'], PDO::PARAM_STR);
$statement->bindParam('email', $_POST['email'], PDO::PARAM_STR);

$statement->execute();

$id = $db->lastInsertId();

echo 'successfully created contact with id ' . $id;

echo '<br /><br />';
echo '<a href="index.php">back to contacts</a>';
