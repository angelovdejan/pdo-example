<?php

$user = 'root';
$password = 'root';

$db = new PDO('mysql:host=localhost;dbname=contacts', $user, $password);

$sql = 'SELECT * FROM contacts WHERE id = :id';

$statement = $db->prepare($sql);
$statement->bindParam('id', $_GET['id'], PDO::PARAM_INT);

$statement->execute();

$result = $statement->fetch(PDO::FETCH_ASSOC);

?>

ID: <?php echo $result['id']; ?><br />
Name: <?php echo $result['name']; ?><br />
Email: <?php echo $result['email']; ?><br />

<br />
<a href="index.php">back to contacts</a>
