<?php

$user = 'root';
$password = 'root';

$db = new PDO('mysql:host=localhost;dbname=contacts', $user, $password);

$sql = 'DELETE FROM contacts WHERE id = :id';

$statement = $db->prepare($sql);
$statement->bindParam('id', $_GET['id'], PDO::PARAM_INT);

$statement->execute();

echo 'successfully deleted';

echo '<br /><br />';
echo '<a href="index.php">back to contacts</a>';
